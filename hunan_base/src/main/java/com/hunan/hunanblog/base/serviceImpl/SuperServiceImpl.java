package com.hunan.hunanblog.base.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hunan.hunanblog.base.mapper.SuperMapper;
import com.hunan.hunanblog.base.service.SuperService;


/**
 * @description SuperService 实现类（ 泛型：M 是  mapper(dao) 对象，T 是实体 ）
 * @author 胡南
 * @date 2022/1/12 22:26
 */

public class SuperServiceImpl<M extends SuperMapper<T>, T> extends ServiceImpl<M, T> implements SuperService<T> {

}
