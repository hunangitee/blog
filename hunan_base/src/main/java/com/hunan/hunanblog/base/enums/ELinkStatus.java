package com.hunan.hunanblog.base.enums;

/**
 * @description 友链状态枚举类
 * @author 胡南
 * @date 2022/1/12 17:28
 */
public class ELinkStatus {

    /**
     * 申请中
     */
    public static final Integer APPLY = 0;

    /**
     * 已发布
     */
    public static final Integer PUBLISH = 1;

    /**
     * 已下架
     */
    public static final Integer NO_PUBLISH = 2;
}