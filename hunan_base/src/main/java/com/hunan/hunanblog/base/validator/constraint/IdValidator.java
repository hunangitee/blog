package com.hunan.hunanblog.base.validator.constraint;

import com.hunan.hunanblog.base.global.Constants;
import com.hunan.hunanblog.utils.StringUtils;
import com.hunan.hunanblog.base.validator.annotion.IdValid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @description ID校验器，主要判断是否为空，并且长度是否为32
 * @author 胡南
 * @date 2022/1/12 22:34
 */
public class IdValidator implements ConstraintValidator<IdValid, String> {


    @Override
    public void initialize(IdValid constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || StringUtils.isBlank(value) || StringUtils.isEmpty(value.trim()) || value.length() != Constants.THIRTY_TWO) {
            return false;
        }
        return true;
    }
}
