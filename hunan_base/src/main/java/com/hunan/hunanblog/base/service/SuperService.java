package com.hunan.hunanblog.base.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @description Service父类
 * @author 胡南
 * @date 2022/1/12 22:25
 */
public interface SuperService<T> extends IService<T> {

}
