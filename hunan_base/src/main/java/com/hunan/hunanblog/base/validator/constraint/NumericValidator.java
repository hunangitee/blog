package com.hunan.hunanblog.base.validator.constraint;

import com.hunan.hunanblog.utils.StringUtils;
import com.hunan.hunanblog.base.validator.annotion.Numeric;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @description 判断是否为数字【校验器】
 * @author 胡南
 * @date 2022/1/12 22:36
 */
public class NumericValidator implements ConstraintValidator<Numeric, String> {
    @Override
    public void initialize(Numeric constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || StringUtils.isBlank(value)) {
            return false;
        }
        if (!StringUtils.isNumeric(value)) {
            return false;
        }
        return true;
    }
}
