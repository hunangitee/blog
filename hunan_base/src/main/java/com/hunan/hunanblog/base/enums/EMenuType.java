package com.hunan.hunanblog.base.enums;

/**
 * @description 菜单类型枚举类
 * @author 胡南
 * @date 2022/1/12 17:29
 */
public class EMenuType {
    /**
     * 菜单
     */
    public static final int MENU = 0;

    /**
     * 按钮
     */
    public static final int BUTTON = 1;
}