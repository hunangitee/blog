package com.hunan.hunanblog.base.enums;

/**
 * @description 原创状态枚举类
 * @author 胡南
 * @date 2022/1/12 17:29
 */
public class EOriginal {

    /**
     * 原创
     */
    public static final String ORIGINAL = "1";

    /**
     * 非原创
     */
    public static final String UNORIGINAL = "0";


}
