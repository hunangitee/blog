package com.hunan.hunanblog.base.enums;

/**
 * @description 开启状态枚举类
 * @author 胡南
 * @date 2022/1/12 17:29
 */
public class EOpenStatus {
    /**
     * 关闭
     */
    public static final String CLOSE = "0";
    /**
     * 开启
     */
    public static final String OPEN = "1";

    /**
     * 关闭
     */
    public static final Integer CLOSE_STATUS = 0;
    /**
     * 开启
     */
    public static final Integer OPEN_STATUS = 1;
}
