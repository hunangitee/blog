package com.hunan.hunanblog.base.enums;

/**
 * @description 平台枚举类
 * @author 胡南
 * @date 2022/1/12 17:30
 */
public enum PlatformEnum {
    /**
     * Admin端 和 Web端
     */
    ADMIN, WEB
}
