package com.hunan.hunanblog.base.enums;

/**
 * @description 搜索模式枚举类
 * @author 胡南
 * @date 2022/1/12 17:30
 */
public class ESearchModel {

    /**
     * SQL搜索
     */
    public static final String SQL = "0";

    /**
     * ElasticSearch搜索
     */
    public static final String ES = "1";

    /**
     * Solr搜索
     */
    public static final String SOLR = "2";
}
