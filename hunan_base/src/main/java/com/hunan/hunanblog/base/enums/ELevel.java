package com.hunan.hunanblog.base.enums;

/**
 * @description 推荐等级枚举类
 * @author 胡南
 * @date 2022/1/12 17:28
 */
public class ELevel {
    /**
     * 正常的
     */
    public static final int NORMAL = 0;

    /**
     * 一级推荐
     */
    public static final int FIRST = 1;

    /**
     * 二级推荐
     */
    public static final int SECOND = 2;

    /**
     * 三级推荐
     */
    public static final int THIRD = 3;

    /**
     * 四级推荐
     */
    public static final int FOURTH = 4;
}