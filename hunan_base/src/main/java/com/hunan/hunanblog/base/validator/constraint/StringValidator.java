package com.hunan.hunanblog.base.validator.constraint;

import com.hunan.hunanblog.utils.StringUtils;
import com.hunan.hunanblog.base.validator.annotion.NotBlank;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
/**
 * @description 判断是否为空字符串【校验器】
 * @author 胡南
 * @date 2022/1/12 22:36
 */
public class StringValidator implements ConstraintValidator<NotBlank, String> {
    @Override
    public void initialize(NotBlank constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || StringUtils.isBlank(value) || StringUtils.isEmpty(value.trim())) {
            return false;
        }
        return true;
    }
}
