package com.hunan.hunanblog.base.vo;

import com.hunan.hunanblog.base.validator.annotion.IdValid;
import com.hunan.hunanblog.base.validator.group.Delete;
import com.hunan.hunanblog.base.validator.group.Update;
import lombok.Data;

/**
 * @description BaseVO   view object 表现层 基类对象
 * @author 胡南
 * @date 2022/1/12 22:37
 */
@Data
public class BaseVO<T> extends PageInfo<T> {

    /**
     * 唯一UID
     */
    @IdValid(groups = {Update.class, Delete.class})
    private String uid;

    private Integer status;
}
