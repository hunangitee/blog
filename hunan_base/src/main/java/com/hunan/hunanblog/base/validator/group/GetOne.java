package com.hunan.hunanblog.base.validator.group;

/**
 * @description GetOne Group  用于get方法查询时
 * @author 胡南
 * @date 2022/1/12 22:36
 */
public interface GetOne {
}
