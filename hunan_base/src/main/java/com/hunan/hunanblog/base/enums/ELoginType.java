package com.hunan.hunanblog.base.enums;

/**
 * @description 网站登录方式枚举类
 * @author 胡南
 * @date 2022/1/12 17:28
 */
public enum ELoginType {

    /**
     * 账号密码
     */
    PASSWORD("1", "PASSWORD"),

    /**
     * 码云
     */
    GITEE("2", "GITEE"),

    /**
     * GITHUB
     */
    GITHUB("3", "GITHUB"),

    /**
     * QQ
     */
    QQ("4", "QQ"),

    /**
     * 微信
     */
    WECHAT("5", "WECHAT");


    private final String code;
    private final String name;

    ELoginType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}