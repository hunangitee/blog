package com.hunan.hunanblog.base.enums;

/**
 * @description 发布状态枚举类
 * @author 胡南
 * @date 2022/1/12 17:29
 */
public class EPublish {

    /**
     * 发布
     */
    public static final String PUBLISH = "1";

    /**
     * 下架
     */
    public static final String NO_PUBLISH = "0";


}
