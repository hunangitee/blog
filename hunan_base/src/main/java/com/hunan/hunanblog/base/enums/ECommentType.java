package com.hunan.hunanblog.base.enums;

/**
 * @description 评论类型枚举类
 * @author 胡南
 * @date 2022/1/12 17:26
 */
public class ECommentType {

    /**
     * 评论
     */
    public static final Integer COMMENT = 0;

    /**
     * 点赞
     */
    public static final Integer PRAISE = 1;


}
