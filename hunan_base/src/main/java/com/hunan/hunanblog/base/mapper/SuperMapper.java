package com.hunan.hunanblog.base.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @description 父类，注意这个类不要让 mybatis-plus 扫描到！！
 * @author 胡南
 * @date 2022/1/12 22:20
 */
public interface SuperMapper<T> extends BaseMapper<T> {

    // 这里可以放一些公共的方法
}
