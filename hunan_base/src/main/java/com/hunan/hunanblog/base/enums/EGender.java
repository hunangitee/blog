package com.hunan.hunanblog.base.enums;

/**
 * @description  性别枚举类
 * @author 胡南
 * @date 2022/1/12 17:27
 */
public class EGender {

    /**
     * 未知
     */
    public static final String UNKNOWN = "0";

    /**
     * 男
     */
    public static final String MALE = "1";

    /**
     * 女
     */
    public static final String FEMALE = "2";
}