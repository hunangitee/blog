package com.hunan.hunanblog.base.enums;

/**
 * @description 文件显示优先级 枚举类
 * @author 胡南
 * @date 2022/1/12 17:26
 */
public class EFilePriority {
    /**
     * 本地文件存储
     */
    public static final String LOCAL = "0";
    /**
     * 七牛云对象存储
     */
    public static final String QI_NIU = "1";

    /**
     * Minio对象存储
     */
    public static final String MINIO = "2";
    /**
     * tomcat外部存储
     */
    public static final String TOMCAT = "3";

}
