package com.hunan.hunanblog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.hunan.hunanblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * @description 收藏表
 * @author 胡南
 * @date 2022/1/12 23:14
 */
@Data
@TableName("t_collect")
public class Collect extends SuperEntity<Collect> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户的uid
     */
    private String userUid;

    /**
     * 博客的uid
     */
    private String blogUid;
}
