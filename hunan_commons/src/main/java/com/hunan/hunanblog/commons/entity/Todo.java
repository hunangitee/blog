package com.hunan.hunanblog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.hunan.hunanblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * @description 代办事项表
 * @author 胡南
 * @date 2022/1/12 23:57
 */
@Data
@TableName("t_todo")
public class Todo extends SuperEntity<Todo> {

    private static final long serialVersionUID = 1L;

    /**
     * 内容
     */
    private String text;

    /**
     * 管理员UID
     */
    private String adminUid;

    /**
     * 表示事项是否完成
     */
    private Boolean done;
}
