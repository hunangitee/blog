package com.hunan.hunanblog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.hunan.hunanblog.base.entity.SuperEntity;
import lombok.Data;


/**
 * @description 参数配置表
 * @author 胡南
 * @date 2022/1/12 23:57
 */
@Data
@TableName("t_sys_params")
public class SysParams extends SuperEntity<SysParams> {

    /**
     * 参数名称
     */
    private String paramsName;

    /**
     * 参数键名
     */
    private String paramsKey;

    /**
     * 参数键值
     */
    private String paramsValue;

    /**
     * 参数类型，是否系统内置（1：是，0：否）
     */
    private Integer paramsType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 排序字段
     */
    private Integer sort;
}
