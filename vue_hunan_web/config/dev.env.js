'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',

  VUE_HUNAN_WEB: '"http://localhost:8484"',
  PICTURE_API: '"http://42.192.81.240:8607/hunan-picture"',
	WEB_API: '"http://42.192.81.240:8607/hunan-web"',
  SEARCH_API: '"http://42.192.81.240:8607/hunan-search"',

})
