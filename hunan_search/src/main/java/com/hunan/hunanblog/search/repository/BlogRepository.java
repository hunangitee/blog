package com.hunan.hunanblog.search.repository;

import com.hunan.hunanblog.search.pojo.ESBlogIndex;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @description BlogRepository操作类
 *             在ElasticsearchRepository中我们可以使用Not Add Like Or Between等关键词自动创建查询语句
 * @author 胡南
 * @date 2022/1/21 14:50
 */
public interface BlogRepository extends ElasticsearchRepository<ESBlogIndex, String> {
}
