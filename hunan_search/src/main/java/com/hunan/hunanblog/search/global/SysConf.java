package com.hunan.hunanblog.search.global;

import com.hunan.hunanblog.base.global.BaseSysConf;

/**
 * @description 系统常量
 * @author 胡南
 * @date 2022/1/17 16:34
 */
public final class SysConf extends BaseSysConf {

    public final static String TITLE = "title";
    public final static String SUMMARY = "summary";
    public final static String NAME = "name";
    public final static String CONTENT = "content";
    public final static String AVATAR = "avatar";
    public final static String ERROR = "error";
    public static final String ADMIN = "admin";
    public static final String BLOG = "blog";
    public static final String EMAIL = "email";

    // 用于第三方登录
    public static final String GITHUB = "github";
    public static final String GITEE = "gitee";
    public static final String PIC_URL = "picUrl";
    public static final String NICKNAME = "nickname";
    public static final String UUID = "uuid";
    public static final String SOURCE = "source";

}

