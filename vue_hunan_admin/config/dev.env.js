'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',

  //开发环境
  ADMIN_API: '"http://42.192.81.240:8607/hunan-admin"',
  PICTURE_API: '"http://42.192.81.240:8607/hunan-picture"',
  WEB_API: '"http://42.192.81.240:8607/hunan-web"',
  Search_API: '"http://42.192.81.240:8607/hunan-search"',
  FILE_API: '"http://42.192.81.240:8600/"',
  BLOG_WEB_URL: '"http://42.192.81.240:8484"',
  ELASTIC_SEARCH: '"http://42.192.81.240:5601"',
})
