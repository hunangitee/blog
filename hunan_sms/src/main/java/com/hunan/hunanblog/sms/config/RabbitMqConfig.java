package com.hunan.hunanblog.sms.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description RabbitMQ配置文件------可用于自动生成队列和交换机
 * @author 胡南
 * @date 2022/1/13 0:46
 */
@Configuration
public class RabbitMqConfig {

    public static final String HUNAN_BLOG = "hunan.blog";
    public static final String HUNAN_EMAIL = "hunan.email";
    public static final String HUNAN_SMS = "hunan.sms";
    public static final String EXCHANGE_DIRECT = "exchange.direct";
    public static final String ROUTING_KEY_BLOG = "hunan.blog";
    public static final String ROUTING_KEY_EMAIL = "hunan.email";
    public static final String ROUTING_KEY_SMS = "hunan.sms";

    /**
     * 声明交换机
     */
    @Bean(EXCHANGE_DIRECT)
    public Exchange EXCHANGE_DIRECT() {
        // 声明路由交换机，durable:在rabbitmq重启后，交换机还在
        return ExchangeBuilder.directExchange(EXCHANGE_DIRECT).durable(true).build();
    }

    /**
     * 声明Blog队列
     *
     * @return
     */
    @Bean(HUNAN_BLOG)
    public Queue HUNAN_BLOG() {
        return new Queue(HUNAN_BLOG);
    }

    /**
     * 声明Email队列
     *
     * @return
     */
    @Bean(HUNAN_EMAIL)
    public Queue HUNAN_EMAIL() {
        return new Queue(HUNAN_EMAIL);
    }

    /**
     * 声明SMS队列
     *
     * @return
     */
    @Bean(HUNAN_SMS)
    public Queue HUNAN_SMS() {
        return new Queue(HUNAN_SMS);
    }

    /**
     * hunan.blog 队列绑定交换机，指定routingKey
     *
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding BINDING_QUEUE_INFORM_BLOG(@Qualifier(HUNAN_BLOG) Queue queue, @Qualifier(EXCHANGE_DIRECT) Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_BLOG).noargs();
    }

    /**
     * hunan.mail 队列绑定交换机，指定routingKey
     *
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding BINDING_QUEUE_INFORM_EMAIL(@Qualifier(HUNAN_EMAIL) Queue queue, @Qualifier(EXCHANGE_DIRECT) Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_EMAIL).noargs();
    }

    /**
     * hunan.sms 队列绑定交换机，指定routingKey
     *
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding BINDING_QUEUE_INFORM_SMS(@Qualifier(HUNAN_SMS) Queue queue, @Qualifier(EXCHANGE_DIRECT) Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY_SMS).noargs();
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
