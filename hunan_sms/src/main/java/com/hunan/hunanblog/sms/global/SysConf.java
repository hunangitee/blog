package com.hunan.hunanblog.sms.global;

import com.hunan.hunanblog.base.global.BaseSysConf;

/**
 * @description 系统常量
 * @author 胡南
 * @date 2022/1/13 0:45
 */
public final class SysConf extends BaseSysConf {

}
