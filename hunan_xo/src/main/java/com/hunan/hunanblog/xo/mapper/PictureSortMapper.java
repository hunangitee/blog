package com.hunan.hunanblog.xo.mapper;

import com.hunan.hunanblog.commons.entity.PictureSort;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 图片分类表 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:53
 */
public interface PictureSortMapper extends SuperMapper<PictureSort> {

}
