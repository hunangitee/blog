package com.hunan.hunanblog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hunan.hunanblog.commons.entity.SysDictType;
import com.hunan.hunanblog.xo.vo.SysDictTypeVO;
import com.hunan.hunanblog.base.service.SuperService;

import java.util.List;

/**
 * @description 字典类型 服务类
 * @author 胡南
 * @date 2022/1/13 0:59
 */
public interface SysDictTypeService extends SuperService<SysDictType> {
    /**
     * 获取字典类型列表
     *
     * @param sysDictTypeVO
     * @return
     */
    public IPage<SysDictType> getPageList(SysDictTypeVO sysDictTypeVO);

    /**
     * 新增字典类型
     *
     * @param sysDictTypeVO
     */
    public String addSysDictType(SysDictTypeVO sysDictTypeVO);

    /**
     * 编辑字典类型
     *
     * @param sysDictTypeVO
     */
    public String editSysDictType(SysDictTypeVO sysDictTypeVO);

    /**
     * 批量删除字典类型
     *
     * @param sysDictTypeVOList
     */
    public String deleteBatchSysDictType(List<SysDictTypeVO> sysDictTypeVOList);
}
