package com.hunan.hunanblog.xo.service.impl;

import com.hunan.hunanblog.commons.entity.CommentReport;
import com.hunan.hunanblog.xo.mapper.CommentReportMapper;
import com.hunan.hunanblog.xo.service.CommentReportService;
import com.hunan.hunanblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @description 评论举报表 服务实现类
 * @author 胡南
 * @date 2022/1/13 1:01
 */
@Service
public class CommentReportServiceImpl extends SuperServiceImpl<CommentReportMapper, CommentReport> implements CommentReportService {

}
