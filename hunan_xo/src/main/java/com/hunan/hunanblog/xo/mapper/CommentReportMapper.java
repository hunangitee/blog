package com.hunan.hunanblog.xo.mapper;

import com.hunan.hunanblog.commons.entity.CommentReport;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 评论举报表 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:52
 */
public interface CommentReportMapper extends SuperMapper<CommentReport> {

}
