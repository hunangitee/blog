package com.hunan.hunanblog.xo.mapper;


import com.hunan.hunanblog.commons.entity.SysParams;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 参数配置 Mapper接口
 * @author 胡南
 * @date 2022/1/13 0:54
 */
public interface SysParamsMapper extends SuperMapper<SysParams> {

}
