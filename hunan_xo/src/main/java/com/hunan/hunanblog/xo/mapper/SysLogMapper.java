package com.hunan.hunanblog.xo.mapper;


import com.hunan.hunanblog.commons.entity.SysLog;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 操作日志 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:54
 */
public interface SysLogMapper extends SuperMapper<SysLog> {

}
