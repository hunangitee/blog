package com.hunan.hunanblog.xo.service;

import com.hunan.hunanblog.commons.entity.SystemConfig;
import com.hunan.hunanblog.xo.vo.SystemConfigVO;
import com.hunan.hunanblog.base.service.SuperService;

import java.util.List;

/**
 * @description 系统配置表 服务类
 * @author 胡南
 * @date 2022/1/13 0:59
 */
public interface SystemConfigService extends SuperService<SystemConfig> {

    /**
     * 获取系统配置
     *
     * @return
     */
    public SystemConfig getConfig();

    /**
     * 通过Key前缀清空Redis缓存
     *
     * @param key
     * @return
     */
    public String cleanRedisByKey(List<String> key);

    /**
     * 修改系统配置
     *
     * @param systemConfigVO
     * @return
     */
    public String editSystemConfig(SystemConfigVO systemConfigVO);

    /**
     * 获取系统配置中的搜索模式
     * @return
     */
    public String getSearchModel();

}
