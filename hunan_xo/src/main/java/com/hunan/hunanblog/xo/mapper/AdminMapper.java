package com.hunan.hunanblog.xo.mapper;

import com.hunan.hunanblog.commons.entity.Admin;
import com.hunan.hunanblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @description 管理员表 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:51
 */
public interface AdminMapper extends SuperMapper<Admin> {

    /**
     * 通过uid获取管理员
     *
     * @return
     */
    public Admin getAdminByUid(@Param("uid") String uid);
}
