package com.hunan.hunanblog.xo.service;

import com.hunan.hunanblog.commons.entity.WebConfig;
import com.hunan.hunanblog.xo.vo.WebConfigVO;
import com.hunan.hunanblog.base.service.SuperService;

/**
 * @description 网站配置表 服务类
 * @author 胡南
 * @date 2022/1/13 1:00
 */
public interface WebConfigService extends SuperService<WebConfig> {

    /**
     * 获取网站配置
     *
     * @return
     */
    WebConfig getWebConfig();

    /**
     * 获取网站名称
     * @return
     */
    String getWebSiteName();

    /**
     * 通过显示列表获取配置
     *
     * @return
     */
    WebConfig getWebConfigByShowList();

    /**
     * 修改网站配置
     *
     * @param webConfigVO
     * @return
     */
    String editWebConfig(WebConfigVO webConfigVO);

    /**
     * 是否开启该登录方式【账号密码、码云、Github、QQ、微信】
     * @param loginType
     * @return
     */
    Boolean isOpenLoginType(String loginType);
}
