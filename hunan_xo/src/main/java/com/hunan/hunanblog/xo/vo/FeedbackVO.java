package com.hunan.hunanblog.xo.vo;

import com.hunan.hunanblog.base.validator.annotion.NotBlank;
import com.hunan.hunanblog.base.validator.group.Insert;
import com.hunan.hunanblog.base.validator.group.Update;
import com.hunan.hunanblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

/**
 * @description 反馈表
 * @author 胡南
 * @date 2022/1/13 1:09
 */
@ToString
@Data
public class FeedbackVO extends BaseVO<FeedbackVO> {

    /**
     * 用户uid
     */
    private String userUid;

    /**
     * 标题
     */
    @NotBlank(groups = {Insert.class, Update.class})
    private String title;

    /**
     * 反馈的内容
     */
    @NotBlank(groups = {Insert.class, Update.class})
    private String content;

    /**
     * 回复
     */
    private String reply;

    /**
     * 反馈状态： 0：已开启  1：进行中  2：已完成  3：已拒绝
     */
    private Integer feedbackStatus;

}
