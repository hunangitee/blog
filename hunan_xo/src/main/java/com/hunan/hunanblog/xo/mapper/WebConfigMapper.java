package com.hunan.hunanblog.xo.mapper;


import com.hunan.hunanblog.commons.entity.WebConfig;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 网站配置表 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:55
 */
public interface WebConfigMapper extends SuperMapper<WebConfig> {

}
