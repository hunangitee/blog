package com.hunan.hunanblog.xo.service.impl;

import com.hunan.hunanblog.commons.entity.Collect;
import com.hunan.hunanblog.xo.mapper.CollectMapper;
import com.hunan.hunanblog.xo.service.CollectService;
import com.hunan.hunanblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @description 收藏表 服务实现类
 * @author 胡南
 * @date 2022/1/13 1:01
 */
@Service
public class CollectServiceImpl extends SuperServiceImpl<CollectMapper, Collect> implements CollectService {

}
