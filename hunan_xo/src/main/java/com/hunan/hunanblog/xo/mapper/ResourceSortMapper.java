package com.hunan.hunanblog.xo.mapper;

import com.hunan.hunanblog.commons.entity.ResourceSort;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 资源分类表 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:53
 */
public interface ResourceSortMapper extends SuperMapper<ResourceSort> {

}
