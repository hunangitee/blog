package com.hunan.hunanblog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hunan.hunanblog.commons.entity.Subject;
import com.hunan.hunanblog.xo.vo.SubjectVO;
import com.hunan.hunanblog.base.service.SuperService;

import java.util.List;

/**
 * @description 专题表 服务类
 * @author 胡南
 * @date 2022/1/13 0:58
 */
public interface SubjectService extends SuperService<Subject> {

    /**
     * 获取专题列表
     *
     * @param subjectVO
     * @return
     */
    public IPage<Subject> getPageList(SubjectVO subjectVO);

    /**
     * 新增专题
     *
     * @param subjectVO
     */
    public String addSubject(SubjectVO subjectVO);

    /**
     * 编辑专题
     *
     * @param subjectVO
     */
    public String editSubject(SubjectVO subjectVO);

    /**
     * 批量删除专题
     *
     * @param subjectVOList
     */
    public String deleteBatchSubject(List<SubjectVO> subjectVOList);

}
