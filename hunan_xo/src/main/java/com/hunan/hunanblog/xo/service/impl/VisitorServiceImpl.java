package com.hunan.hunanblog.xo.service.impl;

import com.hunan.hunanblog.commons.entity.Visitor;
import com.hunan.hunanblog.xo.mapper.VisitorMapper;
import com.hunan.hunanblog.xo.service.VisitorService;
import com.hunan.hunanblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @description 博主表 服务实现类
 * @author 胡南
 * @date 2022/1/13 1:05
 */
@Service
public class VisitorServiceImpl extends SuperServiceImpl<VisitorMapper, Visitor> implements VisitorService {

}
