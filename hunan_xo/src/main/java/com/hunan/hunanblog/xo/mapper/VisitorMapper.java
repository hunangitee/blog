package com.hunan.hunanblog.xo.mapper;

import com.hunan.hunanblog.commons.entity.Visitor;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 游客表 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:54
 */
public interface VisitorMapper extends SuperMapper<Visitor> {

}
