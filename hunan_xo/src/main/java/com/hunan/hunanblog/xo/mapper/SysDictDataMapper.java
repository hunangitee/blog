package com.hunan.hunanblog.xo.mapper;


import com.hunan.hunanblog.commons.entity.SysDictData;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 字典数据 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:53
 */
public interface SysDictDataMapper extends SuperMapper<SysDictData> {

}
