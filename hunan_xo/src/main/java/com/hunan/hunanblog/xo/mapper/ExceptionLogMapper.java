package com.hunan.hunanblog.xo.mapper;

import com.hunan.hunanblog.commons.entity.ExceptionLog;
import com.hunan.hunanblog.base.mapper.SuperMapper;


/**
 * @description 类信息
 * @author 胡南
 * @date 2022/1/13 0:52
 */
public interface ExceptionLogMapper extends SuperMapper<ExceptionLog> {

}
