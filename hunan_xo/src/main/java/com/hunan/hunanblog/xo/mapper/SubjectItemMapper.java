package com.hunan.hunanblog.xo.mapper;

import com.hunan.hunanblog.commons.entity.SubjectItem;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 专题Item Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:53
 */
public interface SubjectItemMapper extends SuperMapper<SubjectItem> {

}
