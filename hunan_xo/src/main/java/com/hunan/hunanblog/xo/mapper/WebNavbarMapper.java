package com.hunan.hunanblog.xo.mapper;

import com.hunan.hunanblog.commons.entity.WebNavbar;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 门户页导航栏 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:55
 */
public interface WebNavbarMapper extends SuperMapper<WebNavbar> {

}
