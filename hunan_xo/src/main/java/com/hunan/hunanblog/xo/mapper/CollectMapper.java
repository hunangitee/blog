package com.hunan.hunanblog.xo.mapper;

import com.hunan.hunanblog.commons.entity.Collect;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 收藏表 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:52
 */
public interface CollectMapper extends SuperMapper<Collect> {

}
