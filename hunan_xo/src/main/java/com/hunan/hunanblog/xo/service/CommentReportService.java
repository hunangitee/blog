package com.hunan.hunanblog.xo.service;

import com.hunan.hunanblog.commons.entity.CommentReport;
import com.hunan.hunanblog.base.service.SuperService;

/**
 * @description 评论举报表 服务类
 * @author 胡南
 * @date 2022/1/13 0:57
 */
public interface CommentReportService extends SuperService<CommentReport> {

}
