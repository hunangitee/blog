package com.hunan.hunanblog.xo.mapper;


import com.hunan.hunanblog.commons.entity.Todo;
import com.hunan.hunanblog.base.enums.EStatus;
import com.hunan.hunanblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @description 待办事项表 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:54
 */
public interface TodoMapper extends SuperMapper<Todo> {

    /**
     * 批量更新未删除的代表事项的状态
     *
     * @param done
     */
    @Select("UPDATE t_todo SET done = #{done} WHERE STATUS = " + EStatus.ENABLE + " AND admin_uid = #{adminUid}")
    public void toggleAll(@Param("done") Integer done, @Param("adminUid") String adminUid);
}
