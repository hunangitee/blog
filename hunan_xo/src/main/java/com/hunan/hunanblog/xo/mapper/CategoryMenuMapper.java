package com.hunan.hunanblog.xo.mapper;

import com.hunan.hunanblog.commons.entity.CategoryMenu;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 菜单表 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:52
 */
public interface CategoryMenuMapper extends SuperMapper<CategoryMenu> {

}
