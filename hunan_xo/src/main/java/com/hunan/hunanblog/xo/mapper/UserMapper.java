package com.hunan.hunanblog.xo.mapper;


import com.hunan.hunanblog.commons.entity.User;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 用户表 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:54
 */
public interface UserMapper extends SuperMapper<User> {

}
