package com.hunan.hunanblog.xo.vo;

import com.hunan.hunanblog.base.validator.annotion.NotBlank;
import com.hunan.hunanblog.base.validator.group.Insert;
import com.hunan.hunanblog.base.validator.group.Update;
import com.hunan.hunanblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

/**
 * @description ResourceSortVO
 * @author 胡南
 * @date 2022/1/13 1:09
 */
@ToString
@Data
public class ResourceSortVO extends BaseVO<ResourceSortVO> {

    /**
     * 分类名
     */
    @NotBlank(groups = {Insert.class, Update.class})
    private String sortName;
    /**
     * 分类介绍
     */
    private String content;

    /**
     * 分类图片UID
     */
    private String fileUid;

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * 无参构造方法
     */
    ResourceSortVO() {

    }

}
