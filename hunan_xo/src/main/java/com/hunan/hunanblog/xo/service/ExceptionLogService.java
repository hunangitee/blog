package com.hunan.hunanblog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hunan.hunanblog.commons.entity.ExceptionLog;
import com.hunan.hunanblog.xo.vo.ExceptionLogVO;
import com.hunan.hunanblog.base.service.SuperService;

/**
 * @description 操作异常日志 服务类
 * @author 胡南
 * @date 2022/1/13 0:57
 */
public interface ExceptionLogService extends SuperService<ExceptionLog> {

    /**
     * 获取异常日志列表
     *
     * @param exceptionLogVO
     * @return
     */
    public IPage<ExceptionLog> getPageList(ExceptionLogVO exceptionLogVO);
}
