package com.hunan.hunanblog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hunan.hunanblog.commons.entity.SysLog;
import com.hunan.hunanblog.xo.vo.SysLogVO;
import com.hunan.hunanblog.base.service.SuperService;

/**
 * @description 操作日志 服务类
 * @author 胡南
 * @date 2022/1/13 0:59
 */
public interface SysLogService extends SuperService<SysLog> {

    /**
     * 获取操作日志列表
     *
     * @param sysLogVO
     * @return
     */
    public IPage<SysLog> getPageList(SysLogVO sysLogVO);
}
