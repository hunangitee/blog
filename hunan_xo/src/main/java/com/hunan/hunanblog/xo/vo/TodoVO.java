package com.hunan.hunanblog.xo.vo;

import com.hunan.hunanblog.base.validator.annotion.BooleanNotNULL;
import com.hunan.hunanblog.base.validator.annotion.NotBlank;
import com.hunan.hunanblog.base.validator.group.GetOne;
import com.hunan.hunanblog.base.validator.group.Insert;
import com.hunan.hunanblog.base.validator.group.Update;
import com.hunan.hunanblog.base.vo.BaseVO;
import lombok.Data;


@Data
public class TodoVO extends BaseVO<TodoVO> {

    /**
     * 内容
     */
    @NotBlank(groups = {Insert.class, Update.class})
    private String text;
    /**
     * 表示事项是否完成
     */
    @BooleanNotNULL(groups = {Update.class, GetOne.class})
    private Boolean done;


    /**
     * 无参构造方法，初始化默认值
     */
    TodoVO() {

    }

}
