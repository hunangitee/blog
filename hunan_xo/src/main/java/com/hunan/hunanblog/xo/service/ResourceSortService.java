package com.hunan.hunanblog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hunan.hunanblog.commons.entity.ResourceSort;
import com.hunan.hunanblog.xo.vo.ResourceSortVO;
import com.hunan.hunanblog.base.service.SuperService;

import java.util.List;

/**
 * @description 资源分类表 服务类
 * @author 胡南
 * @date 2022/1/13 0:58
 */
public interface ResourceSortService extends SuperService<ResourceSort> {

    /**
     * 获取资源分类列表
     *
     * @param resourceSortVO
     * @return
     */
    public IPage<ResourceSort> getPageList(ResourceSortVO resourceSortVO);

    /**
     * 新增资源分类
     *
     * @param resourceSortVO
     */
    public String addResourceSort(ResourceSortVO resourceSortVO);

    /**
     * 编辑资源分类
     *
     * @param resourceSortVO
     */
    public String editResourceSort(ResourceSortVO resourceSortVO);

    /**
     * 批量删除资源分类
     *
     * @param resourceSortVOList
     */
    public String deleteBatchResourceSort(List<ResourceSortVO> resourceSortVOList);

    /**
     * 置顶资源分类
     *
     * @param resourceSortVO
     */
    public String stickResourceSort(ResourceSortVO resourceSortVO);
}
