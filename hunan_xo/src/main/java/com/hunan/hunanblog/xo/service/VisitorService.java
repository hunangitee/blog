package com.hunan.hunanblog.xo.service;

import com.hunan.hunanblog.commons.entity.Visitor;
import com.hunan.hunanblog.base.service.SuperService;

/**
 * @description 博主表 服务类
 * @author 胡南
 * @date 2022/1/13 0:59
 */
public interface VisitorService extends SuperService<Visitor> {

}
