package com.hunan.hunanblog.xo.vo;

import com.hunan.hunanblog.base.validator.annotion.NotBlank;
import com.hunan.hunanblog.base.validator.group.Insert;
import com.hunan.hunanblog.base.validator.group.Update;
import com.hunan.hunanblog.base.vo.BaseVO;
import lombok.Data;


@Data
public class TagVO extends BaseVO<TagVO> {

    /**
     * 标签内容
     */
    @NotBlank(groups = {Insert.class, Update.class})
    private String content;

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    /**
     * 无参构造方法，初始化默认值
     */
    TagVO() {

    }

}
