package com.hunan.hunanblog.xo.mapper;


import com.hunan.hunanblog.commons.entity.SysDictType;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description 字典类型 Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:54
 */
public interface SysDictTypeMapper extends SuperMapper<SysDictType> {

}
