package com.hunan.hunanblog.picture.mapper;

import com.hunan.hunanblog.commons.entity.FileSort;
import com.hunan.hunanblog.base.mapper.SuperMapper;

/**
 * @description Mapper 接口
 * @author 胡南
 * @date 2022/1/13 0:31
 */
public interface FileSortMapper extends SuperMapper<FileSort> {

}
