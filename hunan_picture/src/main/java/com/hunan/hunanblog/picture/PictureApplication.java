package com.hunan.hunanblog.picture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.oas.annotations.EnableOpenApi;


@EnableTransactionManagement
@SpringBootApplication
@EnableOpenApi
@EnableDiscoveryClient
@EnableFeignClients("com.hunan.hunanblog.commons.feign")
@ComponentScan(basePackages = {
        "com.hunan.hunanblog.commons.config.feign",
        "com.hunan.hunanblog.commons.handler",
        "com.hunan.hunanblog.commons.config.redis",
        "com.hunan.hunanblog.utils",
        "com.hunan.hunanblog.picture"})
public class PictureApplication {

    public static void main(String[] args) {
        SpringApplication.run(PictureApplication.class, args);
    }
}
