package com.hunan.hunanblog.picture.global;

import com.hunan.hunanblog.base.global.BaseMessageConf;

/**
 * @description 消息通知 继承base基础类
 * @author 胡南
 * @date 2022/1/13 0:26
 */
public final class MessageConf extends BaseMessageConf {

}
