package com.hunan.hunanblog.picture.vo;

import com.hunan.hunanblog.base.vo.BaseVO;
import lombok.Data;

/**
 * @description CommentVO
 * @author 胡南
 * @date 2022/1/13 0:40
 */
@Data
public class StorageVO extends BaseVO<StorageVO> {

    /**
     * 管理员UID
     */
    private String adminUid;

    /**
     * 存储大小
     */
    private long storagesize;
}
