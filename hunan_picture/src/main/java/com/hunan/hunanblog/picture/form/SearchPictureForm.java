package com.hunan.hunanblog.picture.form;

import com.hunan.hunanblog.base.vo.FileVO;
import lombok.Data;

@Data
public class SearchPictureForm extends FileVO {
    private String searchKey;
    private Integer count;
}
