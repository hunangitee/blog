package com.hunan.hunanblog.picture.service;

import com.hunan.hunanblog.commons.entity.FileSort;
import com.hunan.hunanblog.base.service.SuperService;

/**
 * @description 文件分类服务类
 * @author 胡南
 * @date 2022/1/13 0:32
 */
public interface FileSortService extends SuperService<FileSort> {

}
