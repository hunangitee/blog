package com.hunan.hunanblog.picture.service.impl;

import com.hunan.hunanblog.commons.entity.FileSort;
import com.hunan.hunanblog.picture.mapper.FileSortMapper;
import com.hunan.hunanblog.picture.service.FileSortService;
import com.hunan.hunanblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @description 服务实现类
 * @author 胡南
 * @date 2022/1/13 0:33
 */
@Service
public class FileSortServiceImpl extends SuperServiceImpl<FileSortMapper, FileSort> implements FileSortService {

}
