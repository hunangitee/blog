package com.hunan.hunanblog.picture.global;

import com.hunan.hunanblog.base.global.BaseSysConf;

/**
 * @description 系统常量
 * @author 胡南
 * @date 2022/1/13 0:31
 */
public final class SysConf extends BaseSysConf {

    public final static String ID = "id";
    public final static String CREATE_TIME = "createTime";
    public final static String UPDATE_TIME = "updateTime";


}
