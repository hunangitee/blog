package com.hunan.hunanblog.picture.global;

import com.hunan.hunanblog.base.global.BaseSQLConf;

/**
 * @description SQL字段常量
 * @author 胡南
 * @date 2022/1/13 0:26
 */
public final class SQLConf extends BaseSQLConf {


    //FileSort表
    public final static String PROJECT_NAME = "project_name";
    public final static String SORT_NAME = "sort_name";
    public final static String ADMIN_UID = "admin_uid";
}
