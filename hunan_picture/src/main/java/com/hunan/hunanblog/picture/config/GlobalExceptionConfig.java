package com.hunan.hunanblog.picture.config;

import com.hunan.hunanblog.base.handler.HandlerExceptionResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description 全局异常处理配置
 * @author 胡南
 * @date 2022/1/17 16:16
 */
@Configuration
public class GlobalExceptionConfig {

    @Bean
    public HandlerExceptionResolver getHandlerExceptionResolver() {
        return new HandlerExceptionResolver();
    }
}
