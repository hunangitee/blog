package com.hunan.hunanblog.admin.config;

import com.hunan.hunanblog.base.handler.HandlerExceptionResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description 全局异常配置类，用base中的HandlerExceptionResolver
 * @author 胡南
 * @date 2022/1/12 14:57
 */
@Configuration
public class GlobalExceptionConfig {

    @Bean
    public HandlerExceptionResolver getHandlerExceptionResolver() {
        return new HandlerExceptionResolver();
    }
}
