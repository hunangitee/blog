package com.hunan.hunanblog.admin.global;

import com.hunan.hunanblog.base.global.BaseRedisConf;

/**
 * @description redis配置，继承base的配置
 * @author 胡南
 * @date 2022/1/12 15:27
 */
public class RedisConf extends BaseRedisConf {

}
