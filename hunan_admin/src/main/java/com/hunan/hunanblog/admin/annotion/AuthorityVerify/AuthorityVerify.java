package com.hunan.hunanblog.admin.annotion.AuthorityVerify;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 自定义权限校验接口
 * @author 胡南
 * @date 2022/1/12 15:32
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface  AuthorityVerify {
    String value() default "";
}