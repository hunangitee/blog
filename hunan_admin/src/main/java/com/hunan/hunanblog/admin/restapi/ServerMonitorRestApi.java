package com.hunan.hunanblog.admin.restapi;

import com.hunan.hunanblog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.hunan.hunanblog.admin.global.SysConf;
import com.hunan.hunanblog.utils.ResultUtil;
import com.hunan.hunanblog.utils.ServerInfo.ServerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 服务监控RestApi（CPU、内存、核心）
 * @author 胡南
 * @date 2022/1/12 15:42
 */

@RestController
@RequestMapping("/monitor")
@Api(value = "服务监控相关接口", tags = {"系统设置相关接口"})
@Slf4j
public class ServerMonitorRestApi {

    @AuthorityVerify
    @ApiOperation(value = "获取服务信息", notes = "获取服务信息")
    @GetMapping("/getServerInfo")
    public String getInfo() {
        ServerInfo server = new ServerInfo();
        server.copyTo();
        return ResultUtil.result(SysConf.SUCCESS, server);
    }

}
