package com.hunan.hunanblog.admin.global;

import com.hunan.hunanblog.base.global.BaseMessageConf;

/**
 * @description 消息通知，基础继承base的配置
 * @author 胡南
 * @date 2022/1/12 15:26
 */
public final class MessageConf extends BaseMessageConf {

}
